#!/usr/bin/python3.5

import httplib2
import os
import sys
import time
import configparser
import vimeo
import youtube_dl

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from tools import argparser, run_flow
from multiprocessing import Pool
from functools import partial


def youtube_authenticate():

    # The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
    # the OAuth 2.0 information for this application, including its client_id and
    # client_secret. You can acquire an OAuth 2.0 client ID and client secret from
    # the {{ Google Cloud Console }} at
    # {{ https://cloud.google.com/console }}.
    # Please ensure that you have enabled the YouTube Data API for your project.
    # For more information about using OAuth2 to access the YouTube Data API, see:
    #   https://developers.google.com/youtube/v3/guides/authentication
    # For more information about the client_secrets.json file format, see:
    #   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
    CLIENT_SECRETS_FILE = yt_client_secrets

    # This variable defines a message to display if the CLIENT_SECRETS_FILE is
    # missing.
    MISSING_CLIENT_SECRETS_MESSAGE = """
    WARNING: Please configure OAuth 2.0

    To make this sample run you will need to populate the client_secrets.json file
    found at:

       %s

    with information from the {{ Cloud Console }}
    {{ https://cloud.google.com/console }}

    For more information about the client_secrets.json file format, please visit:
    https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
    """ % os.path.abspath(os.path.join(os.path.dirname(__file__), CLIENT_SECRETS_FILE))

    # This OAuth 2.0 access scope allows for full read/write access to the authenticated user's account.
    YOUTUBE_READ_WRITE_SCOPE = ['https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtube.force-ssl', 'https://www.googleapis.com/auth/youtubepartner']
    YOUTUBE_API_SERVICE_NAME = "youtube"
    YOUTUBE_API_VERSION = "v3"

    flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE,
                                   message=MISSING_CLIENT_SECRETS_MESSAGE,
                                   scope=YOUTUBE_READ_WRITE_SCOPE)

    storage = Storage("%s-oauth2.json" % sys.argv[0])
    credentials = storage.get()

    if credentials is None or credentials.invalid:
        flags = argparser.parse_args()
        credentials = run_flow(flow, storage, flags)

    return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, http=credentials.authorize(httplib2.Http()))


def youtube_search(keyword, max_results, youtube, page_token=""):

    global remainder

    if int(max_results) > 50:
        remainder = int(max_results) - 50
        max_results = 50
    else:
        remainder = 0

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        q=keyword,
        type="video",
        part="id,snippet",
        pageToken=page_token,
        maxResults=max_results
    ).execute()

    # Add each result to the appropriate list, and then display the lists of
    # matching videos, channels, and playlists.
    for search_result in search_response.get("items", []):
        if search_result["id"]["kind"] == "youtube#video":
            youtube_videos.append(search_result["id"]["videoId"])

    if remainder > 0:
        if 'nextPageToken' in search_response:
            youtube_search(keyword, remainder, youtube, search_response["nextPageToken"])


def youtube_create_playlist(youtube):
    playlist_create_response = youtube.playlists().insert(
        part="snippet,status",
        body=dict(
            snippet=dict(
                title=args.keyword,
                description="Created by VidCrawler for keyword %s" % args.keyword
            ),
            status=dict(
                privacyStatus="public"
            )
        )
    ).execute()

    return playlist_create_response["id"]


def youtube_add_to_playlist(youtube, playlist_id, video_id):

    youtube.playlistItems().insert(
        part="snippet",
        body=dict(
            snippet=dict(
                playlistId=playlist_id,
                resourceId=dict(
                    kind="youtube#video",
                    videoId=video_id
                )
            )
        )
    ).execute()

    return "Video %s added to YouTube playlist %s" % (video_id, playlist_id)


def vimeo_authenticate():
    v = vimeo.VimeoClient(
        token=vimeo_token,
        key=vimeo_client_id,
        secret=vimeo_client_secret)

    return v


def vimeo_search(keyword, max_results, vimeo):
    no_of_videos_to_extract = 100
    if int(max_results) > 100:
        no_of_pages = int(max_results) // 100 + 1
        last_page = int(max_results) % 100
    else:
        no_of_pages = 1
        last_page = int(max_results)

    for i in range(1, no_of_pages + 1):
        k = 1
        vimeo_search_response = vimeo.get('/videos', params={"query": keyword, "fields": "name,uri", "per_page": "100", "page": i})
        # print(vimeo_search_response.headers)

        if i == no_of_pages:
            no_of_videos_to_extract = last_page
        for item in vimeo_search_response.json().get("data", []):
            if k > no_of_videos_to_extract:
                break
            # print(item["uri"][8:])
            vimeo_videos.append(item["uri"][8:])
            k += 1
        if vimeo_search_response.json()["paging"]["next"] is None:
            break


def vimeo_create_album(vimeo):
    params = {"description": "Created by VidCrawler for keyword %s" % args.keyword, "name": args.keyword}
    vimeo_create_album_response = vimeo.post('/me/albums', data=params)
    return vimeo_create_album_response


def vimeo_add_to_album(vimeo, album_id):
    video_list = ', '.join(vimeo_videos)
    params = {"videos": "%s" % video_list}
    add_to_album_response = vimeo.put('/me/albums/%s/videos' % album_id, data=params)
    # print(add_to_album_response.headers)
    if add_to_album_response.status_code != 201:
        print("Failed to add videos to Vimeo album %s" % album_id)


def crawl_youtube():
    global youtube_playlist
    try:
        print("Crawling YouTube for %s" % args.keyword)
        youtube_auth = youtube_authenticate()
        youtube_search(args.keyword, args.max_results, youtube_auth)
        # print(len(youtube_videos))
        if len(youtube_videos) > 0:
            playlist_id = youtube_create_playlist(youtube_auth)
            for youtube_video in youtube_videos:
                youtube_add_to_playlist(youtube_auth, playlist_id, youtube_video)
            youtube_playlist = "https://www.youtube.com/playlist?list=%s" % playlist_id
            if is_download_youtube:
                to_download.append('youtube')
            print("YouTube playlist created: https://www.youtube.com/playlist?list=%s" % playlist_id)
        else:
            print("No results found for %s in YouTube" % args.keyword)
    except HttpError as e:
        print("An HTTP error %d occurred:\n%s" % (e.resp.status, e.content))


def crawl_vimeo():
    global vimeo_album
    try:
        print("Crawling Vimeo for %s" % args.keyword)
        vimeo_auth = vimeo_authenticate()
        vimeo_search(args.keyword, args.max_results, vimeo_auth)
        if len(vimeo_videos) > 0:
            vimeo_create_album_response = vimeo_create_album(vimeo_auth)
            # print(vimeo_create_album_response.status_code)
            if vimeo_create_album_response.status_code == 201:
                album_uri = vimeo_create_album_response.json()['uri']
                index = album_uri.find("/albums/") + 8
                vimeo_album_id = album_uri[index:]
                vimeo_add_to_album(vimeo_auth, vimeo_album_id)
                vimeo_album = "https://vimeo.com/album/%s" % vimeo_album_id
                if is_download_vimeo:
                    to_download.append('vimeo')
                print("Vimeo album created: https://vimeo.com/album/%s" % vimeo_album_id)
            else:
                print("Failed to create Vimeo album")
        else:
            print("No results found for %s in Vimeo" % args.keyword)

        # if len(vimeo_videos) > 0:
        #     album_id = vimeo_create_album()
        #     for vimeo_video in vimeo_videos:
        #         print(vimeo_add_to_album(album_id, vimeo_video))
        #     print("Vimeo album created: https://vimeo.com/album/%s" % album_id)
        # else:
        #     print("No results found for %s in Vimeo" % args.keyword)

    except HttpError as e:
        print("An HTTP error %d occurred:\n%s" % (e.resp.status, e.content))


def download_progress_hook(d):
    if d['status'] == 'finished':
        print('Downloaded %s' % d['filename'])


def download_videos(template, quality, url):
    # time.sleep(15)
    # print("url: %s quality: %s template: %s" % (url, quality, template))
    print("Downloading %s" % url)
    ydl_opts = {
        'format': 'best[ext=mp4,height<=%s]/best' % quality,
        'quiet': True,
        'outtmpl': template,
        'progress_hooks': [download_progress_hook],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])


def download(site):
    if site == "youtube":
        download_youtube(youtube_playlist)
    else:
        download_vimeo(vimeo_album)


if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read("config.ini")
    no_of_videos = config.get("common", "number_of_videos_to_crawl")
    is_crawl_youtube = config.getboolean("common", "crawl_youtube")
    is_crawl_vimeo = config.getboolean("common", "crawl_vimeo")
    is_download_youtube = config.getboolean("common", "download_youtube")
    is_download_vimeo = config.getboolean("common", "download_vimeo")
    yt_client_secrets = config.get("youtube", "client_secrets_file")
    vimeo_token = config.get("vimeo", "token")
    vimeo_client_id = config.get("vimeo", "client_id")
    vimeo_client_secret = config.get("vimeo", "client_secret")

    youtube_download_directory = config.get("youtube", "download_directory")
    vimeo_download_directory = config.get("vimeo", "download_directory")
    youtube_quality = config.get("youtube", "maximum_video_quality")
    vimeo_quality = config.get("vimeo", "maximum_video_quality")

    argparser.add_argument("keyword", help="Keyword to search for")
    argparser.add_argument("--max-results", help="Maximum number of results to produce", default=no_of_videos)
    argparser.add_argument("--youtube-download-directory", help="Directory path to download YouTube videos", default=youtube_download_directory)
    argparser.add_argument("--vimeo-download-directory", help="Directory path to download Vimeo videos", default=vimeo_download_directory)
    args = argparser.parse_args()
    remainder = 0
    youtube_videos = []
    vimeo_videos = []
    to_download = []
    youtube_playlist = None
    vimeo_album = None
    start = time.time()
    # Handling directory paths ending with a slash
    if args.youtube_download_directory[-1] == "/":
        args.youtube_download_directory = args.youtube_download_directory[:-1]
    if args.vimeo_download_directory[-1] == "/":
        args.vimeo_download_directory = args.vimeo_download_directory[:-1]

    youtube_outtmpl = args.youtube_download_directory + '/%(playlist_index)s - %(title)s.%(ext)s'
    vimeo_outtmpl = args.vimeo_download_directory + '/%(playlist_index)s - %(title)s.%(ext)s'

    download_youtube = partial(download_videos, youtube_outtmpl, youtube_quality)
    download_vimeo = partial(download_videos, vimeo_outtmpl, vimeo_quality)

    if not is_crawl_youtube and not is_crawl_youtube:
        print("Crawling is disabled for both YouTube & Vimeo. Please enable at least one channel to crawl.")
    else:
        if is_crawl_youtube:
            crawl_youtube()
        if is_crawl_vimeo:
            crawl_vimeo()

    if len(to_download) > 0:
        with Pool(len(to_download)) as p:
            p.map(download, to_download)
